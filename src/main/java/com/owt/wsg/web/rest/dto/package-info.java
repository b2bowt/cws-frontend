/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.owt.wsg.web.rest.dto;
