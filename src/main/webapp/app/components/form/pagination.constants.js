(function() {
    'use strict';

    angular
        .module('cwSfrontendApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
