(function () {
    'use strict';

    angular
        .module('cwSfrontendApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
